#gioca partita 2048

#prende come input la direzione e lo schema precedente con le caratteristiche alla fine
#direzione: 0 verso alto, 1 verso sinistra, 2 verso destra, 3 verso basso

import random
import sys

def sposta_dx(schema):
    return [[i for i in riga if i==0] + [j for j in riga if j!=0] for riga in schema]

def sposta_sx(schema):
    return [[i for i in riga if i!=0] + [j for j in riga if j==0] for riga in schema]

def sposta_basso(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    colonne_nuove=sposta_dx(colonne) #per spostare verso il basso devo spostare verso dx le colonne
    return [[riga[i] for riga in colonne_nuove] for i in range(len(colonne_nuove))] #ritraspongo la matrice

def sposta_alto(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    colonne_nuove=sposta_sx(colonne) #per spostare verso l'alto devo spostare verso sx le colonne
    return [[riga[i] for riga in colonne_nuove] for i in range(len(colonne_nuove))] #ritraspongo la matrice

def lista_adiacenti(riga, colonna, schema): #funzione per restituire la lista dei valori delle caselle adiacenti
    dim=len(schema)
    lista=[]
    for i in (-1, 1):
        if 0<=riga+i<dim:
            lista.append(schema[riga+i][colonna])
        if 0<=colonna+i<dim:
            lista.append(schema[riga][colonna+i])
    return lista

def controlla_perdita(schema): #controlla l'impossibilita di fare altre mosse
    for riga in range(len(schema)):
        for colonna in range(len(schema)):
            adiacenti=lista_adiacenti(riga, colonna, schema)
            if 0 in adiacenti or schema[riga][colonna] in adiacenti:
                return False
    return True

def controlla_vincita(schema, massima_potenza):
    for riga in schema:
        if massima_potenza in riga:
            return True
    return False

def semplifica_riga(riga): #funzione per semplificare una riga nel caso di 2 numeri uguali, funziona da sinistra a destra
    indice=1 #parte a contare dal secondo termine per vedere se quello precedente e' uguale
    copia=riga[:]
    while indice<len(copia):
        if copia[indice]!=0 and copia[indice]==copia[indice-1]:#se vede che si possono sommare perche' stesso nuemro
            copia=copia[:indice-1]+[copia[indice]+1]+copia[indice+1:]+[0] #modifica la lista in modo da aggiungere uno 0 alla fine
        indice+=1 #incrementa sempre di 1 per evitare che sommi 2 volte la stessa casella
    return copia

def mossa_dx(schema): #mossa completa di movimento verso dx
    tabella=sposta_dx(schema)
    return [semplifica_riga(riga[::-1])[::-1] for riga in tabella] #ho invertito 2 volte per applicare semplifica_riga correttamente

def mossa_sx(schema): #mossa completa verso sx
    tabella=sposta_sx(schema)
    return [semplifica_riga(riga) for riga in tabella] #non inverto in questo caso

def mossa_basso(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    tabella=mossa_dx(colonne) #devo praticamente fare la mossa a dx dello schema trasposto
    return [[riga[i] for riga in tabella] for i in range(len(tabella))] #ritraspongo la matrice

def mossa_alto(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    tabella=mossa_sx(colonne) #devo praticamente fare la mossa a dx dello schema trasposto
    return [[riga[i] for riga in tabella] for i in range(len(tabella))] #ritraspongo la matrice

direzioni=[mossa_alto, mossa_sx, mossa_dx, mossa_basso]

#ricavo lo schema dall'input

verso=int(sys.argv[1]) #il primo parametro e' il verso di giocata
vecchio=sys.argv[2].split('\n') #divido gia' l'output vecchio precedente con separatore di riga
difficolta=int(vecchio[0])
pot_max=int(vecchio[1])
matrice=[[int(i) for i in riga.split(',')] for riga in vecchio[2:]] #a questo punto dovrebbe esserci gia lo schema pronto
dim=len(matrice)
nuovo=direzioni[verso](matrice)
if nuovo==matrice:
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
elif controlla_vincita(nuovo, pot_max):
    print 'V'
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
else:
    celle_vuote=[(riga, colonna) for riga in range(dim) for colonna in range(dim) if nuovo[riga][colonna]==0]
    scelte=random.sample(celle_vuote, min(difficolta, len(celle_vuote)))
    for pos in scelte:
        nuovo[pos[0]][pos[1]]=random.randint(1,2)
    #a questo punto nuovo dovrebbe avere i numeri inseriti
    if controlla_perdita(nuovo):
        print 'P'
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
