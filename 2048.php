<?php

function genera_2048($diff=1, $potenza=10, $dim=4){
$comando="python2.7 genera_2048.py $diff $potenza $dim";
return trim(escapeshellarg($comando));
}

function sost_2048($text){
return str_replace(","," ",str_replace("0","⬜",str_replace("1","🔍",str_replace("2","🔮",str_replace("3","🔦",str_replace("4","💡",str_replace("5","⚔️",str_replace("6","📿",str_replace("7","🗝️",str_replace("8","🎁",str_replace("9","📝",str_replace("10","💎",str_replace("11","🖌️",str_replace("12","🔒",str_replace("13","🛡️",$text)))))))))))))));
}

function stampa_2048($text){
$tabella=explode("\n", $text);
$indice=2;
if ($text[0]=="V" or $text[0]=="P"){
$indice=3;
}
$mess="";
for ($i=$indice; $i<count($tabella); $i++){
$mess.=sost_2048($tabella[$i])."\n";
}
return trim($mess);
}

function partita_2048($direzione, $vecchio){
$comando="python2.7 gioca_2048.py $direzione $vecchio";
$output=trim(escapeshellarg($comando));
$stato_partita=0; //indica che la partita puo' continuare
if($output[0]=="V"){
$stato_partita=1;}
elseif($output[0]=="P"){
$stato_partita=-1;}
return array($stato_partita, $output);
}

?>