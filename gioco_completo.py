#gioco compelto 2048

import sys
import subprocess

def stampa(tabella):
    print '_'+'_'*2*len(tabella)
    for i in tabella:
        print '|'+i.replace(',', ' ').replace('0', ' ')+'|'

def rif():
    print '\n'
    print 'w'.center(11)
    print 'a'.center(5)+' '+'d'.center(5)
    print 's'.center(11)
    print '\n'

#prendo gli input

diff=raw_input('Inserire la difficolta (quanti numeri vuoi che compaiano ogni volta?): ')
max_pot=raw_input('Inserire la massima potenza di 2 a cui si vuole arrivare per vincere: ')
dimensione=raw_input('Inserire la dimensione dello schema: ')

out=subprocess.check_output(['python', 'genera_2048.py', diff, max_pot, dimensione])[:-1]#l'ultimo carattere sara' un \n

stampa(out.split('\n')[2:])

while out[0] not in ('V','P'):
    rif()
    spostamento=raw_input('inserire la direzione: ').replace('s','3').replace('a','1').replace('d','2').replace('w','0')
    out=subprocess.check_output(['python', 'gioca_2048.py', spostamento, out])[:-1]
    if out[0] in ('V','P'):
        stampa(out.split('\n')[3:])
    else:
        stampa(out.split('\n')[2:])

if out[0]=='V':
    print 'complimenti hai vinto'
else:
    print 'hai perso'
