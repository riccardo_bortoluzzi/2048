import random
import sys

def sposta_dx(schema):
    return [[i for i in riga if i==0] + [j for j in riga if j!=0] for riga in schema]

def sposta_sx(schema):
    return [[i for i in riga if i!=0] + [j for j in riga if j==0] for riga in schema]

def sposta_basso(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    colonne_nuove=sposta_dx(colonne) #per spostare verso il basso devo spostare verso dx le colonne
    return [[riga[i] for riga in colonne_nuove] for i in range(len(colonne_nuove))] #ritraspongo la matrice

def sposta_alto(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    colonne_nuove=sposta_sx(colonne) #per spostare verso l'alto devo spostare verso sx le colonne
    return [[riga[i] for riga in colonne_nuove] for i in range(len(colonne_nuove))] #ritraspongo la matrice

def lista_adiacenti(riga, colonna, schema): #funzione per restituire la lista dei valori delle caselle adiacenti
    dim=len(schema)
    lista=[]
    for i in (-1, 1):
        if 0<=riga+i<dim:
            lista.append(schema[riga+i][colonna])
        if 0<=colonna+i<dim:
            lista.append(schema[riga][colonna+i])
    return lista

def controlla_perdita(schema): #controlla l'impossibilita di fare altre mosse
    for riga in range(len(schema)):
        for colonna in range(len(schema)):
            adiacenti=lista_adiacenti(riga, colonna, schema)
            if 0 in adiacenti or schema[riga][colonna] in adiacenti:
                return False
    return True

def controlla_vincita(schema, massima_potenza):
    for riga in schema:
        if massima_potenza in riga:
            return True
    return False

def semplifica_riga(riga): #funzione per semplificare una riga nel caso di 2 numeri uguali, funziona da sinistra a destra
    indice=1 #parte a contare dal secondo termine per vedere se quello precedente e' uguale
    copia=riga[:]
    while indice<len(copia):
        if copia[indice]!=0 and copia[indice]==copia[indice-1]:#se vede che si possono sommare perche' stesso nuemro
            copia=copia[:indice-1]+[copia[indice]+1]+copia[indice+1:]+[0] #modifica la lista in modo da aggiungere uno 0 alla fine
        indice+=1 #incrementa sempre di 1 per evitare che sommi 2 volte la stessa casella
    return copia

def mossa_dx(schema): #mossa completa di movimento verso dx
    tabella=sposta_dx(schema)
    return [semplifica_riga(riga[::-1])[::-1] for riga in tabella] #ho invertito 2 volte per applicare semplifica_riga correttamente

def mossa_sx(schema): #mossa completa verso sx
    tabella=sposta_sx(schema)
    return [semplifica_riga(riga) for riga in tabella] #non inverto in questo caso

def mossa_basso(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    tabella=mossa_dx(colonne) #devo praticamente fare la mossa a dx dello schema trasposto
    return [[riga[i] for riga in tabella] for i in range(len(tabella))] #ritraspongo la matrice

def mossa_alto(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    tabella=mossa_sx(colonne) #devo praticamente fare la mossa a dx dello schema trasposto
    return [[riga[i] for riga in tabella] for i in range(len(tabella))] #ritraspongo la matrice

def stampa_schema(schema, pot_max): 
    lunghezza = len(str(2**pot_max)) #cosi ho la lunghezza massima dei numeri che appaiono
    for riga in schema:
        lista = [] #lista dove salvero in formato stringa i vari quadrati
        for c in riga:
            stringa = str(2**c).center(lunghezza) if c != 0 else " "*lunghezza
            lista.append(stringa)
        print ' '.join(lista)

direzioni={"w":mossa_alto, "a":mossa_sx, "d":mossa_dx, "s":mossa_basso} #dizionario con chiave i caratteri w,a,s,d che indica rispettivamente alto, sinistra, basso, destra

#ricavo lo schema dall'input

difficolta=int(raw_input("Inserire quanti numeri inserire ad ogni iterazione: ")) #indica quanti numeri inserire nello schema ad ogni iterazione
pot_massima=int(raw_input("Inserire il valore della potenza di 2 a cui si vuole che il gioco termini: ")) #indica la potenza di 2 a cui terminare il gioco
dim=int(raw_input("Inserire la dimensione del lato del quadrato dello schema: ")) #indica la dimensione dello schema quadrato

schema=[[0 for i in range(dim)] for j in range(dim)] #schema composto da soli 0
caselle=[(riga, colonna) for riga in range(dim) for colonna in range(dim)] #crea una lista delle caselle da sostituire con i numeri
scelta=random.sample(caselle, difficolta) #sceglie un nuemro di caselle pari alla difficolta
for pos in scelta:
    schema[pos[0]][pos[1]]=random.randint(1,2)
    
#a questo punto schema e' inizializzato
stampa_schema(schema, pot_massima)

while not(controlla_vincita(schema, pot_massima)) and not(controlla_perdita(schema)): # se si puo continuare a giocare
    verso = raw_input("Inserire la direzione verso la quale si vuole giocare (valori possibili: w, a, s, d):")
    while verso not in ("w","a","s","d"): #controllo per verificare che il verso inserito sia corretto
        print "Verso non valido"
        verso = raw_input("Inserire la direzione verso la quale si vuole giocare (valori possibili: w, a, s, d):")
    schema = direzioni[verso](schema) #eseguo la mossa
    if not(controlla_vincita(schema, pot_massima)) and not(controlla_perdita(schema)):
        caselle_vuote = [(r,c) for r in range(dim) for c in range(dim) if schema[r][c] == 0] #caselle vuote
        scelta = random.sample(caselle_vuote, difficolta)
        for (r, c) in scelta:
            schema[r][c] = random.randint(1,2)
    stampa_schema(schema, pot_massima)

#se esce significa che si ha perso oppure vinto
if controlla_vincita(schema, pot_massima):
    print "Complimenti hai vinto"
else:
    print "Mi dispiace ma hai perso"
            
'''
verso=int(sys.argv[1]) #il primo parametro e' il verso di giocata
vecchio=sys.argv[2].split('\n') #divido gia' l'output vecchio precedente con separatore di riga
difficolta=int(vecchio[0])
pot_max=int(vecchio[1])
matrice=[[int(i) for i in riga.split(',')] for riga in vecchio[2:]] #a questo punto dovrebbe esserci gia lo schema pronto
dim=len(matrice)
nuovo=direzioni[verso](matrice)
if nuovo==matrice:
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
elif controlla_vincita(nuovo, pot_max):
    print 'V'
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
else:
    celle_vuote=[(riga, colonna) for riga in range(dim) for colonna in range(dim) if nuovo[riga][colonna]==0]
    scelte=random.sample(celle_vuote, min(difficolta, len(celle_vuote)))
    for pos in scelte:
        nuovo[pos[0]][pos[1]]=random.randint(1,2)
    #a questo punto nuovo dovrebbe avere i numeri inseriti
    if controlla_perdita(nuovo):
        print 'P'
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
'''