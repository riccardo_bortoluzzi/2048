#genera 2048
#prende come input la difficolta, la potenza massima e la dimensione dello schema

import random
import sys

difficolta=int(sys.argv[1]) #indica quanti numeri inserire nello schema ad ogni iterazione
pot_massima=int(sys.argv[2]) #indica la potenza di 2 a cui terminare il gioco
dim=int(sys.argv[3]) #indica la dimensione dello schema quadrato
schema=[[0 for i in range(dim)] for j in range(dim)] #schema composto da soli 0
caselle=[(riga, colonna) for riga in range(dim) for colonna in range(dim)] #crea una lista delle caselle da sostituire con i numeri
scelta=random.sample(caselle, difficolta) #sceglie un nuemro di caselle pari alla difficolta
for pos in scelta:
    schema[pos[0]][pos[1]]=random.randint(1,2)

#stampo tutte le caratteristiche e poi lo schema
print difficolta
print pot_massima
#prime 2 righe per dare indicazioni ai prossimi script
#ogni termine separato da una virgola
for riga in schema:
    print ','.join([str(j) for j in riga])

