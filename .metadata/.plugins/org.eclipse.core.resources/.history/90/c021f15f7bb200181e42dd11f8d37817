import random
from Tkinter import *
import tkMessageBox

def sposta_dx(schema):
    return [[i for i in riga if i==0] + [j for j in riga if j!=0] for riga in schema]

def sposta_sx(schema):
    return [[i for i in riga if i!=0] + [j for j in riga if j==0] for riga in schema]

def sposta_basso(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    colonne_nuove=sposta_dx(colonne) #per spostare verso il basso devo spostare verso dx le colonne
    return [[riga[i] for riga in colonne_nuove] for i in range(len(colonne_nuove))] #ritraspongo la matrice

def sposta_alto(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    colonne_nuove=sposta_sx(colonne) #per spostare verso l'alto devo spostare verso sx le colonne
    return [[riga[i] for riga in colonne_nuove] for i in range(len(colonne_nuove))] #ritraspongo la matrice

def lista_adiacenti(riga, colonna, schema): #funzione per restituire la lista dei valori delle caselle adiacenti
    dim=len(schema)
    lista=[]
    for i in (-1, 1):
        if 0<=riga+i<dim:
            lista.append(schema[riga+i][colonna])
        if 0<=colonna+i<dim:
            lista.append(schema[riga][colonna+i])
    return lista

def controlla_perdita(schema): #controlla l'impossibilita di fare altre mosse
    for riga in range(len(schema)):
        for colonna in range(len(schema)):
            adiacenti=lista_adiacenti(riga, colonna, schema)
            if 0 in adiacenti or schema[riga][colonna] in adiacenti:
                return False
    return True

def controlla_vincita(schema):
    global pot_massima
    for riga in schema:
        if pot_massima in riga:
            return True
    return False

def semplifica_riga(riga): #funzione per semplificare una riga nel caso di 2 numeri uguali, funziona da sinistra a destra
    indice=1 #parte a contare dal secondo termine per vedere se quello precedente e' uguale
    copia=riga[:]
    while indice<len(copia):
        if copia[indice]!=0 and copia[indice]==copia[indice-1]:#se vede che si possono sommare perche' stesso nuemro
            copia=copia[:indice-1]+[copia[indice]+1]+copia[indice+1:]+[0] #modifica la lista in modo da aggiungere uno 0 alla fine
        indice+=1 #incrementa sempre di 1 per evitare che sommi 2 volte la stessa casella
    return copia

def mossa_dx(schema): #mossa completa di movimento verso dx
    tabella=sposta_dx(schema)
    return [semplifica_riga(riga[::-1])[::-1] for riga in tabella] #ho invertito 2 volte per applicare semplifica_riga correttamente

def mossa_sx(schema): #mossa completa verso sx
    tabella=sposta_sx(schema)
    return [semplifica_riga(riga) for riga in tabella] #non inverto in questo caso

def mossa_basso(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    tabella=mossa_dx(colonne) #devo praticamente fare la mossa a dx dello schema trasposto
    return [[riga[i] for riga in tabella] for i in range(len(tabella))] #ritraspongo la matrice

def mossa_alto(schema):
    colonne=[[riga[i] for riga in schema] for i in range(len(schema))] #creo la lista delle colonne
    tabella=mossa_sx(colonne) #devo praticamente fare la mossa a dx dello schema trasposto
    return [[riga[i] for riga in tabella] for i in range(len(tabella))] #ritraspongo la matrice

def stampa_schema(schema):
    global dim 
    #lunghezza = len(str(2**pot_massima)) #cosi ho la lunghezza massima dei numeri che appaiono
    lunghezza = max([len(str(2**i))for riga in schema for i in riga]) #massima lunghezza dei numeri che appaiono
    #dimensione = len(schema)
    print (" " + "_"*lunghezza)*dim
    for riga in schema:
        lista = [] #lista dove salvero in formato stringa i vari quadrati
        for c in riga:
            stringa = str(2**c).center(lunghezza) if c != 0 else " "*lunghezza
            lista.append(stringa)
        print "|" + ' '.join(lista) + "|"
    print (" " + "_"*lunghezza)*dim

def esegui_giocata(schema, verso):
    global dim, difficolta,pot_massima #variabili globali che non vengono modificate durante l'esecuzione del gioco
    direzioni={"w":mossa_alto, "a":mossa_sx, "d":mossa_dx, "s":mossa_basso} #dizionario con chiave i caratteri w,a,s,d che indica rispettivamente alto, sinistra, basso, destra
    controllo = 1
    nuovo = direzioni[verso](schema) #eseguo la mossa
    caselle_vuote = [(r,c) for r in range(dim) for c in range(dim) if nuovo[r][c] == 0] #caselle vuote
    if len(caselle_vuote) < difficolta:
        controllo = 0 #non si hanno piu le caselle per continuare, partita persa
    if nuovo != schema and not(controlla_vincita(nuovo)) and not(controlla_perdita(nuovo)) and controllo ==1:
    #if not(controlla_vincita(schema, pot_massima)) and not(controlla_perdita(schema)):
        scelta = random.sample(caselle_vuote, difficolta)
        for (r, c) in scelta:
            nuovo[r][c] = random.randint(1,2)
    return (nuovo, controllo) #ritorno la tupla che contiene informazioni sul controllo e il nuovo schema

def nuova_partita(dim, difficolta): #funzione per generare lo schema, volendo anche dim e difficolta si sarebbero potuti prendere da variabili globali
    schema=[[0 for i in range(dim)] for j in range(dim)] #schema composto da soli 0
    caselle=[(riga, colonna) for riga in range(dim) for colonna in range(dim)] #crea una lista delle caselle da sostituire con i numeri
    scelta=random.sample(caselle, difficolta) #sceglie un nuemro di caselle pari alla difficolta
    for pos in scelta:
        schema[pos[0]][pos[1]]=random.randint(1,2)
    return schema

#funzioni per la grafica
def pulsante_nuova_partita():
    global difficolta, dim, schema
    schema = nuova_partita(dim, difficolta)
    crea_grafica(schema)

def pulsante_alto():
    global schema, vincita
    (schema,controllo) = esegui_giocata(schema, "w")
    crea_grafica(schema)
    if vincita == 0 and controlla_vincita(schema):
        vincita = 1
        richiesta = tkMessageBox.askyesno(title = "Complimenti", message = "Complimenti, hai vinto\nVuoi cominciare \nuna nuova partita?")
        if richiesta:
            pulsante_nuova_partita()
        #tkMessageBox.showinfo(title="Vittoria!", message="Complimenti, hai vinto\nPuoi continuare a giocare")
    elif controllo == 0:
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di aggiungere\nnuove caselle\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
    elif controlla_perdita(schema):
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di eseguire\n mosse valide\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
        
def pulsante_sinistra():
    global schema, vincita
    (schema,controllo) = esegui_giocata(schema, "a")
    crea_grafica(schema)
    if vincita == 0 and controlla_vincita(schema):
        vincita = 1
        richiesta = tkMessageBox.askyesno(title = "Complimenti", message = "Complimenti, hai vinto\nVuoi cominciare \nuna nuova partita?")
        if richiesta:
            pulsante_nuova_partita()
        #tkMessageBox.showinfo(title="Vittoria!", message="Complimenti, hai vinto\nPuoi continuare a giocare")
    elif controllo == 0:
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di aggiungere\nnuove caselle\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
    elif controlla_perdita(schema):
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di eseguire\n mosse valide\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
        
def pulsante_destra():
    global schema, vincita
    (schema,controllo) = esegui_giocata(schema, "d")
    crea_grafica(schema)
    if vincita == 0 and controlla_vincita(schema):
        vincita = 1
        richiesta = tkMessageBox.askyesno(title = "Complimenti", message = "Complimenti, hai vinto\nVuoi cominciare \nuna nuova partita?")
        if richiesta:
            pulsante_nuova_partita()
        #tkMessageBox.showinfo(title="Vittoria!", message="Complimenti, hai vinto\nPuoi continuare a giocare")
    elif controllo == 0:
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di aggiungere\nnuove caselle\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
    elif controlla_perdita(schema):
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di eseguire\n mosse valide\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
        
def pulsante_basso():
    global schema, vincita
    (schema,controllo) = esegui_giocata(schema, "s")
    crea_grafica(schema)
    if vincita == 0 and controlla_vincita(schema):
        vincita = 1
        richiesta = tkMessageBox.askyesno(title = "Complimenti", message = "Complimenti, hai vinto\nVuoi cominciare \nuna nuova partita?")
        if richiesta:
            pulsante_nuova_partita()
        #tkMessageBox.showinfo(title="Vittoria!", message="Complimenti, hai vinto\nPuoi continuare a giocare")
    elif controllo == 0:
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di aggiungere\nnuove caselle\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
    elif controlla_perdita(schema):
        tkMessageBox.showerror(title="Sconfitta", message="Hai perso\nPer impossibilita' di eseguire\n mosse valide\ncliccando su 'OK' \nricomincerai una nuova partita")
        pulsante_nuova_partita()
        
def pulsante_uscita():
    conferma = tkMessageBox.askokcancel(title = "Uscita", message = "Sei sicuro \ndi voler uscire?")
    if conferma:
        scheda.destroy()

        
def crea_grafica(schema):
    for label in finestra.grid_slaves(): #questo comando serve per eliminare eventuale grafica
        label.grid_forget()
    global finestra, dim #variabili globali
    for r in range(dim):
        #inserisco le limitazioni a sinistra e destra
        Label(finestra, text = "|").grid(row = r+1)
        Label(finestra, text = "|").grid(row = r+1, column = dim+1) 
        #inserisco le limitazioni in alto e in basso
        max_lung = max([len(str(2**i))for riga in schema for i in riga]) #massima lunghezza dei numeri che appaiono
        Label(finestra, text = "_"*max_lung).grid(row = 0, column = r+1) #metto r+1 cosi sfrutto il fatto che lo schema e' quadrato
        Label(finestra, text = "_"*max_lung).grid(row = dim+1, column = r+1) #metto r+1 cosi sfrutto il fatto che lo schema e' quadrato
        for c in range(dim):
            carattere = str(2**schema[r][c]).center(max_lung) if schema[r][c]!= 0 else " ".center(max_lung)
            Label(finestra, text=carattere).grid(row = r+1, column = c+1)
    #creo ora i pulsanti di movimento, metto una riga bianca
    Label(finestra, text = "").grid(row=dim+2)
    Button(finestra, text="Up", command = pulsante_alto).grid(row = dim+3, columnspan = (dim+2))
    Button(finestra, text="Lt", command = pulsante_sinistra).grid(row = dim+4, column = 0, columnspan = (dim+2)/2)
    Button(finestra, text="Rt", command = pulsante_destra).grid(row = dim+4, column = dim/2+1, columnspan = (dim+2)/2)
    Button(finestra, text="Dn", command = pulsante_basso).grid(row = dim+5, columnspan = (dim+2))
    #creo ora i pulsanti di nuova partita e uscita, salto sempre una riga
    Label(finestra, text = "").grid(row=dim+6)
    Button(finestra, text="New", command = pulsante_nuova_partita).grid(row = dim+7, column = 0, columnspan = (dim+2)/2)
    Button(finestra, text="Exit", command = pulsante_uscita).grid(row = dim+7, column = dim/2+1, columnspan = (dim+2)/2)



#ricavo lo schema dall'input

difficolta=int(raw_input("Inserire quanti numeri inserire ad ogni iterazione: ")) #indica quanti numeri inserire nello schema ad ogni iterazione
pot_massima=int(raw_input("Inserire il valore della potenza di 2 a cui si vuole che il gioco termini (per terminare al raggiungimento di 2048 inserire 11): ")) #indica la potenza di 2 a cui terminare il gioco
dim=int(raw_input("Inserire la dimensione del lato del quadrato dello schema: ")) #indica la dimensione dello schema quadrato
grafica = raw_input("Vuoi giocare in modalita' grafica? (s/n): ").lower()
'''
schema=[[0 for i in range(dim)] for j in range(dim)] #schema composto da soli 0
caselle=[(riga, colonna) for riga in range(dim) for colonna in range(dim)] #crea una lista delle caselle da sostituire con i numeri
scelta=random.sample(caselle, difficolta) #sceglie un nuemro di caselle pari alla difficolta
for pos in scelta:
    schema[pos[0]][pos[1]]=random.randint(1,2)
'''

schema = nuova_partita(dim, difficolta)

controllo = 1 #variabile che serve per verificare se il numero di celle libere e' sufficiente per la difficolta

#a questo punto schema e' inizializzato


if grafica != "s":
    stampa_schema(schema)
    while not(controlla_vincita(schema)) and not(controlla_perdita(schema)) and controllo ==1: # se si puo continuare a giocare
        verso = raw_input("Inserire la direzione verso la quale si vuole giocare (valori possibili: w, a, s, d):")
        while verso not in ("w","a","s","d"): #controllo per verificare che il verso inserito sia corretto
            print "Verso non valido"
            verso = raw_input("Inserire la direzione verso la quale si vuole giocare (valori possibili: w, a, s, d):")
        '''
        nuovo = direzioni[verso](schema) #eseguo la mossa
        caselle_vuote = [(r,c) for r in range(dim) for c in range(dim) if nuovo[r][c] == 0] #caselle vuote
        if len(caselle_vuote) < difficolta:
            controllo = 0 #non si hanno piu le caselle per continuare, partita persa
        if nuovo != schema and not(controlla_vincita(nuovo, pot_massima)) and not(controlla_perdita(nuovo)) and controllo ==1:
        #if not(controlla_vincita(schema, pot_massima)) and not(controlla_perdita(schema)):
            scelta = random.sample(caselle_vuote, difficolta)
            for (r, c) in scelta:
                nuovo[r][c] = random.randint(1,2)
        '''
        (schema, controllo) = esegui_giocata(schema, verso)
        stampa_schema(schema)
    
    #se esce significa che si ha perso oppure vinto
    if controlla_vincita(schema):
        print "Complimenti hai vinto"
    else:
        print "Mi dispiace ma hai perso"
else:
    vincita = 0
    scheda = Tk()
    scheda.title("2048")
    scheda.geometry("400x600")
    
    finestra = Frame(scheda)
    finestra.pack()
    
    #con i seguenti comandi s' possibile giocare nella modalita grfica anche semplicemente premendo w,a,s,d, ho utilizzato lambda function altrimenti mi passava la lettera come argomento delle funzioni e ricadevo in errore
    scheda.bind('w', lambda event: pulsante_alto())
    scheda.bind('a', lambda event: pulsante_sinistra())
    scheda.bind('s', lambda event: pulsante_basso())
    scheda.bind('d', lambda event: pulsante_destra())
    
    crea_grafica(schema)
    
    finestra.mainloop()            
'''
verso=int(sys.argv[1]) #il primo parametro e' il verso di giocata
vecchio=sys.argv[2].split('\n') #divido gia' l'output vecchio precedente con separatore di riga
difficolta=int(vecchio[0])
pot_max=int(vecchio[1])
matrice=[[int(i) for i in riga.split(',')] for riga in vecchio[2:]] #a questo punto dovrebbe esserci gia lo schema pronto
dim=len(matrice)
nuovo=direzioni[verso](matrice)
if nuovo==matrice:
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
elif controlla_vincita(nuovo, pot_max):
    print 'V'
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
else:
    celle_vuote=[(riga, colonna) for riga in range(dim) for colonna in range(dim) if nuovo[riga][colonna]==0]
    scelte=random.sample(celle_vuote, min(difficolta, len(celle_vuote)))
    for pos in scelte:
        nuovo[pos[0]][pos[1]]=random.randint(1,2)
    #a questo punto nuovo dovrebbe avere i numeri inseriti
    if controlla_perdita(nuovo):
        print 'P'
    #stampo tutte le caratteristiche e poi lo schema
    print difficolta
    print pot_max
    #prime 2 righe per dare indicazioni ai prossimi script
    #ogni termine separato da una virgola
    for riga in nuovo:
        print ','.join([str(j) for j in riga])
'''